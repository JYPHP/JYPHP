<?php
// +----------------------------------------------------------------------
// | PHP [ JUST YOU ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2017 http://www.jyphp.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: flyingfig <albert_p@foxmail.com>
// +----------------------------------------------------------------------

namespace App\Extend;


use Throwable;

class ApiException extends \Exception
{

    public function __construct($message = "", $code = -6, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}