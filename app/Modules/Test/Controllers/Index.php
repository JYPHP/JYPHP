<?php
// +----------------------------------------------------------------------
// | PHP [ JUST YOU ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2017 http://www.jyphp.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Albert <albert_p@foxmail.com>
// +----------------------------------------------------------------------
namespace Modules\Test\Controllers;

use App\Extend\ExceptionHandler;
use JYPHP\Core\Controller;
use JYPHP\Core\Traits\Controller\Json;

/**
 * Index
 * @package Modules\Test\Controllers
 */
class Index extends Controller
{
    use Json;

    /**
     * @api 测试接口
     */
    public function get()
    {
        return "Hello World";
    }
}