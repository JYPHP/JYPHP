<?php
// +----------------------------------------------------------------------
// | PHP [ JUST YOU ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2017 http://www.jyphp.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Albert <albert_p@foxmail.com>
// +----------------------------------------------------------------------
namespace App\Modules\Test;

use JYPHP\Core\Module;

class TestModules extends Module
{

    public function boot()
    {
        // TODO: Implement boot() method.
    }

    public function register()
    {
        // TODO: Implement register() method.
    }

    /**
     * 模块描述
     * @return string
     */
    public static function description(): string
    {
        return "测试模块";
    }

    /**
     * 模块名
     * @return string
     */
    public static function name(): string
    {
        return "测试模块";
    }

    /**
     * 模块版本
     * @return string
     */
    public static function version(): string
    {
        return "v1.0";
    }

    /**
     * 模块作者
     * @return string
     */
    public static function author(): string
    {
        return "Albert <albert_p@foxmail.com>";
    }
}