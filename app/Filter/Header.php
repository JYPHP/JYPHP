<?php
// +----------------------------------------------------------------------
// | PHP [ JUST YOU ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2017 http://www.jyphp.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Albert <albert_p@foxmail.com>
// +----------------------------------------------------------------------
namespace App\Filter;

use JYPHP\Core\Filter\Abstracts\Filter;
use JYPHP\Core\Http\Request;

class Header extends Filter
{
    public function handle(Request $request, \Closure $next, ...$params)
    {
        response()->header(...$params);
        return $next($request);
    }
}