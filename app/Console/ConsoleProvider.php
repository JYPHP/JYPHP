<?php
// +----------------------------------------------------------------------
// | PHP [ JUST YOU ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2017 http://www.jyphp.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Albert <albert_p@foxmail.com>
// +----------------------------------------------------------------------
namespace App\Console;

use JYPHP\Core\ServiceProvider;

class ConsoleProvider extends ServiceProvider
{
    public function boot()
    {
        $this->commands([
            NoticeServerCommand::class,
            CloudScreenshotsCommand::class
        ]);
    }
}