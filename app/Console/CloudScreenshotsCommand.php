<?php
// +----------------------------------------------------------------------
// | PHP [ JUST YOU ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2017 http://www.jyphp.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Albert <albert_p@foxmail.com>
// +----------------------------------------------------------------------
namespace App\Console;

use App\Models\V9News;
use App\Models\V9NewsData;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use JYPHP\Core\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class CloudScreenshotsCommand extends Command
{
    const KEY = 'cloud_screenshots_pid';

    protected $name = "server:cloud_screen";

    protected $desc = "通知推送服务";

    /**
     * 守护进程
     * @var bool
     */
    protected $daemon = true;

    public function configure()
    {
        parent::configure();
        $this->addArgument(
            "operation",
            InputArgument::REQUIRED,
            "操作类型有: start 开启服务 , dev 开发者模式开启服务 , kill 关闭服务 , restart 重启服务"
        );
    }

    public function handle()
    {
        $operations = [
            'start',
            'dev',
            'kill',
            'restart'
        ];
        $operation = $this->input->getArgument('operation');
        if (!(method_exists($this, $operation) && in_array($operation, $operations))) {
            throw new \Exception("'" . $operation . "'" . "  not operation");
        }
        $this->output->writeln($operation . "ing ...");
        $this->app->call(
            [
                $this,
                $operation
            ]
        );
    }

    public function start()
    {
        if ($this->daemon) {
            \swoole_process::daemon(true, true);
        }
        Cache::forever(self::KEY, posix_getpid());
        while (true) {

        }
    }

    public function kill()
    {
        exec(escapeshellcmd(
            "kill" . " "
            . Cache::get(self::KEY)
        ));
    }

    public function restart()
    {
        $count = V9NewsData::count();
        for ($i = 0; $i < 30; $i++) {
            $p = new \swoole_process(function ($w) use ($count, $i) {
                $limit = $count / 30;
                var_dump($limit);
                V9NewsData::orderBy("id", "desc")->skip($limit * ($i - 1))->take($limit)->chunk(200, function ($datas) {
                    foreach ($datas as $data) {
                        $content = $data->content;
                        $img_src = explode('"', explode("src", $content)[1])[1];
                        if ($img_src) {
                            try {
                                $news = V9News::where("id", $data->id)->first();
                                $news->thumbs = $img_src;
                                $news->save();
                                echo "ok" . " " . $data->id;
                            } catch (\Exception $exception) {
                                echo $exception->getMessage() . "id = " . $data->id;
                            }
                        }
                    }
                });
            }, false, false);
            var_dump($p->start());
        }
    }

    public function dev()
    {
        ini_set("memory_limit","600M");
        $l = 20000;
        $data = [

        ];
        for ($i = 0; $i < $l; $i++) {
            $data[] = [
                "eas_name" => "test_name" . $i,
                "eas_province" => "广东省",
                "eas_city" => "深圳市",
                "eas_region" => "宝安区"
            ];
        }
        unset($l);
        $start_time = microtime(true);
        $z_sql = '';
        DB::beginTransaction();
        foreach ($data as $item) {
            $eas_name = $item['eas_name'];
            $eas_province = $item['eas_province'];
            $eas_city = $item['eas_city'];
            $eas_region = $item['eas_region'];
            $sql = "insert into em_address_school (`eas_name`,`eas_province`,`eas_city`,`eas_region`) VALUES ('$eas_name','$eas_province','$eas_city','$eas_region'); ";
            DB::unprepared($sql);
//            $z_sql .= " " . $sql;
        }
//        DB::unprepared($z_sql);
//        DB::commit();
        $end_time = microtime(true);
        var_dump($end_time - $start_time);
    }
}