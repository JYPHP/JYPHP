<?php
// +----------------------------------------------------------------------
// | PHP [ JUST YOU ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2017 http://www.jyphp.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Albert <albert_p@foxmail.com>
// +----------------------------------------------------------------------
return [
    "default_action" => "index",

    "exception_render" => \App\Extend\ExceptionHandler::class,

    "debug" => true,

    "modules_namespace" => "Modules\\",

    "timezone" => "Asia/Shanghai"
];