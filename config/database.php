<?php
// +----------------------------------------------------------------------
// | PHP [ JUST YOU ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2017 http://www.jyphp.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Albert <albert_p@foxmail.com>
// +----------------------------------------------------------------------
return array(
    'default' => 'em',

    'connections' => [
        'em' => [
            'driver' => 'mysql',
            //            'host' => 'rm-wz9v4g5q1q64145i8o.mysql.rds.aliyuncs.com',
            'host' => 'localhost',
            'database' => 'em',
            //            'database' => 'em_dug',
            'username' => 'root',
            //            'username' => 'albert',
            'password' => 'root',
            //            'password' => 'Albert168',
            'charset' => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix' => 'em_',
//            'options' => [
//                PDO::ATTR_PERSISTENT => true,
//            ]
        ],

        'v9' => [
            'driver' => 'mysql',
            'host' => '5989988ac36a9.sh.cdb.myqcloud.com',
            'port' => '5045',
            'database' => 'dg',
            'username' => 'cdb_outerroot',
            'password' => 'BINGbing520',
            'charset' => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix' => 'v9_'
        ]
    ]
);